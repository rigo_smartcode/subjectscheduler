import 'package:dartson/dartson.dart';
import 'package:subject_scheduler/model/block.dart';

@Entity()
class ScheduleDetail {
  String accesTime;
  String subject;
  String dayName;
  String profesor;
  String room;
  String startTime;
  String endTime;
  bool isPopupVisible = false;
  bool showMenu = true;
  List<Block> blockRange;

  ScheduleDetail(
      {this.accesTime = "",
      this.subject = "",
      this.dayName = "",
      this.profesor = "",
      this.room = "",
      this.startTime = "",
      this.endTime = ""});

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ScheduleDetail &&
          runtimeType == other.runtimeType &&
          startTime == other.startTime &&
          endTime == other.endTime;

  @override
  int get hashCode => startTime.hashCode ^ endTime.hashCode;
}
