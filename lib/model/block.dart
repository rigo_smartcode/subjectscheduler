import 'package:dartson/dartson.dart';
import 'package:subject_scheduler/model/all_model.dart';

@Entity()
class Block {
  ScheduleDetail info;
  Map<String, String> styles;
  int row;
  int column;
  String startTime;
  String endTime;
  String UName;

  Block({this.styles, this.row, this.column, this.startTime, this.endTime});

  String get id {
    return "block_${row}_${column}";
  }

  @override
  String toString() {
    return 'Block{row: $row, column: $column, startTime: $startTime, endTime: $endTime}';
  }
}
