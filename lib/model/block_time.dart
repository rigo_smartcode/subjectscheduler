class BlockTime {
  String time;
  DateTime dateTime;
  bool isOclock;
  bool isAfter9;
  bool isAm;
  Map<String, String> styles;

  BlockTime(
      {this.time,
      this.dateTime,
      this.isOclock: false,
      this.isAfter9: false,
      this.styles,
      this.isAm});

  @override
  String toString() {
    return 'BlockTime{time: $time, dateTime: $dateTime, isOclock: $isOclock, isAfter9: $isAfter9, isAm: $isAm, styles: $styles}';
  }
}
