import 'package:subject_scheduler/model/all_model.dart';

class Day {
  String name;
  Map<String, String> styles;
  String evaluation;
  String appointment;
  bool showAuxPanel;

  List<Block> blocks;
  List<Block> BlockTime;

  Day(
      {this.name,
      this.styles,
      this.evaluation: "Evaluaciones",
      this.appointment: "Citas",
      this.blocks,
      this.BlockTime});
}
