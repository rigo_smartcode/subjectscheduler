import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:angular_forms/angular_forms.dart';
import 'package:subject_scheduler/model/all_model.dart';
import 'package:subject_scheduler/src/comopnents/all_components.dart';

@Component(selector: 'main', templateUrl: 'main_component.html', styleUrls: const [
  'main_component.css'
], directives: const [
  ALL_COMPONENTS,
  CORE_DIRECTIVES,
  materialDirectives,
  formDirectives,
  MaterialAutoSuggestInputComponent,
  MaterialCheckboxComponent,
  MaterialDropdownSelectComponent,
  MaterialIconComponent,
  materialInputDirectives,
  NgFor,
], providers: const [
  materialProviders
], pipes: const [])
class MainComponent implements OnInit {
  List<String> teachers = ["Nicolas Maduro", "José Alfredo Fuentes", "Cesar Oyarzo"];
  List<String> subjects = ["Programacion 1", "Ciencias de la computacion", "Matematica"];
  List<String> rooms = ["401", "506", "304"];
  ScheduleDetail data;
  List<ScheduleDetail> blockDetailList;
  String accesTime;

  ngOnInit() {
    blockDetailList = [];
    data = new ScheduleDetail();
    accesTime = "";
  }

  onRangeSelected(ScheduleDetail data) {
    this.data = data;
    accesTime = "${this.data.startTime} - ${this.data.endTime}";
  }

  @ViewChild("timeScheduler")
  TimeSchedulerComponent timeScheduler;

  @ViewChild("timeChooser")
  TimeChooserComponent timeChooser;

  accept() {
    accesTime = "";
    timeScheduler.commitChanges();
    data = new ScheduleDetail();
  }

  bool get showTeacherClearIncon => data.profesor != null && !data.profesor.isEmpty;

  bool get showSubjectClearIncon => data.subject != null && !data.subject.isEmpty;

  bool get showRoomClearIncon => data.room != null && !data.room.isEmpty;

  //time scheduler

  onTimeCreated(ScheduleDetail detail) {
    print('MainComponent.onTimeSeclected ${detail} ');
  }

  onSchedulerModified(Map<String, List<ScheduleDetail>> detail) {
    detail.forEach((k, v) {
      if (v != null) {
        print('${k} : ${v.map((d) => "${d.startTime} a ${d.endTime}").toList().join(" - ")}');
      }
    });
    //print('MainComponent.onTimeSeclected ${detail} ');
  }
}
