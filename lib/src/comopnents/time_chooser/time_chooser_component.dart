import 'dart:async';
import 'dart:html';
import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:intl/intl.dart';
import 'package:subject_scheduler/model/all_model.dart';
import 'package:dartson/dartson.dart';
import 'package:subject_scheduler/util//humanhash.dart' as hhash;

@Component(
  selector: 'time-chooser',
  styleUrls: const ['time_chooser_component.css'],
  templateUrl: 'time_chooser_component.html',
  directives: const [
    CORE_DIRECTIVES,
    materialDirectives,
    MaterialButtonComponent,
    MaterialPopupComponent,
    PopupSourceDirective,
    MaterialIconComponent,
    MaterialListComponent,
    MaterialListItemComponent,
  ],
  providers: const [materialProviders, popupBindings],
)
class TimeChooserComponent implements OnInit {
  Dartson dson = new Dartson.JSON();

  static const LEFT_BUTTON_CLICK = 0;

  static List<String> daysName = [
    "Lunes",
    "Martes",
    "Miercoles",
    "Jueves",
    "Viernes",
    "Sabado",
    "Domingo"
  ];

  ScheduleDetail schedulerInfo;
  bool showBasicDialog = false;
  List<Block> blocks = [];
  List<Day> bodyDays = [];
  List<Day> footerDays = [];
  List<BlockTime> horarioList = [];

  DivElement schedulerOverlay;

  int blockWidth = 70;
  int blockHeight = 32;
  int firstColWidth = 1;
  int blocksCount = (16 * 4) + 1;
  int scrollLength;

  @ViewChild("calendarScheduler")
  ElementRef calendarScheduler;
  DivElement mainDiv;
  DivElement contentBody;
  DivElement contentHeader;
  DivElement bodyScroll;

  bool showOverlay = false;

  Map<String, String> popUpStyles;

  List<Block> selectedBlocks;
  List<Block> selectedBlocks2;

  bool basicPopupVisible = false;

  //@inputs

  @Input()
  List<ScheduleDetail> scheduleDetailList;
  Map<String, List<ScheduleDetail>> detailMap;

  StreamController _onSchedulerModified = new StreamController.broadcast();

  @Output()
  Stream get onSchedulerModified => _onSchedulerModified.stream;

  StreamController _onTimeCreated = new StreamController.broadcast();

  @Output()
  Stream get onTimeCreated => _onTimeCreated.stream;

  StreamController _onTimeSelected = new StreamController.broadcast();

  @Output()
  Stream get onTimeSelected => _onTimeSelected.stream;

  //end @Inputs

  @override
  ngOnInit() {
    scheduleDetailList = scheduleDetailList != null ? scheduleDetailList : [];
    detailMap = detailMap != null ? detailMap : new Map();
    daysName.forEach((d) => detailMap[d] = []);
    //scheduler container
    mainDiv = calendarScheduler.nativeElement as DivElement;
    contentBody = mainDiv.querySelector(".content-body");
    bodyScroll = contentBody.querySelector(".body-scroll");
    popUpStyles = new Map();
    schedulerInfo =
        new ScheduleDetail(subject: "Ciencias", profesor: "Macos Enriquez", room: "443");

    //initOverlay();

    DateTime startDate = new DateTime(2017, 1, 1, 08, 00);
    for (int i = 0; i < blocksCount; i++) {
      Map<String, String> styles = new Map();

      styles["width"] = "${blockWidth}px";

      if (i == 0) {
        styles["width"] = "${firstColWidth}px";
        horarioList.add(
          new BlockTime(time: '', styles: styles),
        );
      } else {
        String startTime = parseDate(startDate);

        horarioList.add(
          new BlockTime(
              isAm: startTime.contains("AM"),
              time: startTime.replaceAll("AM", "").replaceAll("PM", "").replaceAll(":00", ""),
              dateTime: new DateTime.fromMillisecondsSinceEpoch(startDate.millisecondsSinceEpoch),
              isOclock: (i - 1) % 4 == 0,
              styles: styles),
        );
        startDate = startDate.add(new Duration(minutes: int.parse("15")));
      }
    }
    daysName.asMap().forEach((index, dayName) {
      Map<String, String> styles = new Map();
      styles["height"] = "${blockHeight}px";

      if (index == daysName.length - 1) {
        styles["height"] = "${blockHeight + 3}px";
        styles["border-bottom"] = "1px solid #DADFEA";
      }

      bodyDays.add(new Day(styles: styles, name: dayName, blocks: createBlocks(rowNumber: index)));
    });

    for (int i = 0; i < daysName.length; i++) {
      Map<String, String> styles = new Map();
      if (i == 0) {
        styles["max-width"] = "${firstColWidth}px";
      }
      footerDays.add(new Day(
          styles: new Map(), name: "", blocks: createBlocks(rowNumber: i, withStyle: false)));
    }

    initContentScroll();
    initDND();
  }

  List<Block> createBlocks({int rowNumber, withStyle = true}) {
    List<Block> blocks = [];
    int left = 0;
    for (int i = 0; i < blocksCount; i++) {
      int localBlockWidth = blockWidth;
      //first block 20px width
      if (i == 0) {
        localBlockWidth = firstColWidth;
      }

      Map<String, String> styles = new Map();

      if (withStyle) {
        styles["width"] = "${localBlockWidth}px";
        styles["height"] = "${blockHeight}px";
        styles["position"] = "absolute";
        styles["top"] = "${rowNumber * blockHeight}px";
        styles["left"] = "${left}px";

        String borderStyle = !(i % 4 == 0) ? "dashed" : "solid";
        styles["border-right"] = "1px ${borderStyle} #DADFEA";
      }

      String startTime = null;
      String endTime = null;
      if (horarioList[i].dateTime != null) {
        startTime = parseDate(horarioList[i].dateTime);
        endTime = parseDate(horarioList[i].dateTime.add(new Duration(minutes: 15)));
      }

      blocks.add(new Block(
          styles: styles, row: rowNumber, column: i, startTime: startTime, endTime: endTime));

      left += localBlockWidth;
    }

    return blocks;
  }

  String parseDate(DateTime dt) {
    var formatter = new DateFormat.Hm();
    String formatted = formatter.format(dt);
    return formatted;
  }

  initContentScroll() {
    //scroll length in pixels
    scrollLength = (blocksCount * blockWidth) - blockWidth + firstColWidth;

    DivElement contentBody = mainDiv.querySelector(".content-body");

    if (contentBody == null) return;

    contentBody.style.height = "${(blockHeight + 6.5) * daysName.length}px";
    contentHeader = mainDiv.querySelector(".content-header");

    //set the final scroll lenght
    DivElement bodyScroll = contentBody.querySelector(".body-scroll");
    bodyScroll.style.width = "${scrollLength}px";

    DivElement headerScroll = contentHeader.querySelector(".header-scroll");
    headerScroll.style.width = "${scrollLength}px";
    contentBody.onScroll.listen((e) {
      contentHeader.scrollLeft = contentBody.scrollLeft;
    });

    contentBody.onMouseOver.listen((e) {
      if (isMouseDown) {
        if (e.page.x > contentBody.client.width) {
          contentBody.scrollLeft = contentBody.scrollLeft + 50;
        } else if (e.page.x < 400) {
          contentBody.scrollLeft = contentBody.scrollLeft - 50;
        }
      }
    });
  }

  bool isMouseDown = false;
  int rowStart = -1;
  int colStart = -1;
  int colEnd = -1;

  initDND() {
    if (contentBody == null) return;

    bodyScroll.onMouseDown.listen((e) {
      e.preventDefault();

      if (e.button == LEFT_BUTTON_CLICK &&
          e.target is DivElement &&
          (e.target as DivElement).classes.contains("subject-block")) {
        DivElement block = (e.target as DivElement);

        rowStart = int.parse(block.dataset["row"]);
        colStart = int.parse(block.dataset["column"]);

        setBlockBackgroundColor(block);
        isMouseDown = true;
      }
    });

    bodyScroll.onMouseUp.listen((e) {
      if (isMouseDown) {
        isMouseDown = false;

        int colStartAux = -1;
        if (colEnd < colStart) {
          colStartAux = colStart;
          colStart = colEnd;
          colEnd = colStartAux;
        }
        selectedBlocks = bodyDays[rowStart].blocks.getRange(colStart, colEnd + 1).toList();
        selectedBlocks2 = footerDays[rowStart].blocks.getRange(colStart, colEnd + 1).toList();

/*        print('rowStart $rowStart - '
            'colStart $colStart - '
            'colEnd $colEnd - '
            'selected cols ${selectedBlocks}');*/

        // showInputDialog();
        String dayName = bodyDays[selectedBlocks.first.row].name;

        ScheduleDetail schedulerDetail = new ScheduleDetail();
        schedulerDetail.startTime = selectedBlocks.first.startTime;
        schedulerDetail.endTime = selectedBlocks.last.endTime;
        schedulerDetail.dayName = dayName;
        schedulerDetail.blockRange = dson.decode(dson.encode(selectedBlocks), new Block(), true);

        List times = detailMap[dayName];
        if (times == null) {
          detailMap[dayName] = [schedulerDetail];
        } else {
          times.add(schedulerDetail);
        }

        _onSchedulerModified.add(detailMap);
        _onTimeCreated.add(schedulerDetail);
        onAcceptDialog(schedulerDetail);
      }
    });

    bodyScroll.onMouseOver.listen((e) {
      if (isMouseDown) {
        DivElement block = (e.target as DivElement);
        setBlockBackgroundColor(block);
      }
    });
    bodyScroll.onClick.listen((e) {
      e.preventDefault();
      onEventPanelClick(e.target);
    });
  }

  void onEventPanelClick(Element target) {
    DivElement eventElement = getElementWithEventClass(target);
    if (target is DivElement && eventElement != null) {
      selectNewEventPanel(eventElement);
    }
  }

  void deSelectOldEventPanel(DivElement e) {
    if (lastSelectedUName != null) {
      List<Element> lastSelected = querySelectorAll(".${lastSelectedUName}");
      for (int i = 0; i < lastSelected.length; i++) {
        Element element = lastSelected[i];
        if (i == 0) {
          _getBLockFromSubjectBLockHtml(element)?.info?.showMenu = false;
        }
        element.style.backgroundColor = "#f1f1f1";
      }
    }
  }

  Block _getBLockFromSubjectBLockHtml(Element element) {
    int row = int.parse(element.parent.dataset["row"]);
    int column = int.parse(element.parent.dataset["column"]);
    return bodyDays[row].blocks[column];
  }

  void selectNewEventPanel(DivElement eventElement) {
    String blockUName = eventElement.classes.last;
    if (blockUName != lastSelectedUName) {
      deSelectOldEventPanel(eventElement);
      lastSelectedUName = blockUName;
      document.querySelectorAll(".${blockUName}").asMap().forEach((index, element) {
        if (index == 0) {
          _getBLockFromSubjectBLockHtml(element).info.showMenu = true;
        }
        element.style.backgroundColor = "cyan";
      });
    }
  }

  Element getElementWithEventClass(Element e) {
    if (e == null) {
      return e;
    }
    if (e.classes.contains("event")) {
      return e;
    }
    return getElementWithEventClass(e.parent);
  }

  void fillEventDiv(Block startBlock, int blocksLength) {
    String id = "#block_${startBlock.row}_${startBlock.column}";
    DivElement selectedCol = mainDiv.querySelector(id);
    DivElement event = selectedCol.querySelector(".event");
    event.style.position = "absolute";
    event.style.borderRadius = "2px";
    //  event.style.left = "${selectedCol.style.left}";
    //   event.style.top = "${int.parse(selectedCol.style.top.replaceAll("px", ""))}px";
    event.style.zIndex = "20";
    event.style.height = "${blockHeight - 5}px";
    event.style.marginTop = "2px";
    event.style.width = "${(blockWidth * blocksLength) - 5}px";
    event.style.marginLeft = "2px";
    event.style.backgroundColor = "#F9F9F9";
  }

  String lastSelectedUName = null;

  void deleteEvent(Block block) {
    List times = detailMap[block.info.dayName];
    block.info = null;
    DivElement selectedCol = _getHtmlBlockElement(block);
    DivElement eventElement = selectedCol.querySelector(".event");
    String uName = eventElement.classes.last;
    List<Element> elements = document.querySelectorAll(".${uName}");
    Block firstB = _getBLockFromSubjectBLockHtml(elements.first);
    Block lastB = _getBLockFromSubjectBLockHtml(elements.last);

    times.remove(new ScheduleDetail(startTime: firstB.startTime, endTime: lastB.endTime));
    document.querySelectorAll(".${uName}").asMap().forEach((index, element) {
      int row = int.parse(element.parent.dataset["row"]);
      int col = int.parse(element.parent.dataset["column"]);

      footerDays[row].blocks[col].styles = {};
      element.attributes["style"] = null;
    });

    _onSchedulerModified.add(detailMap);
  }

  void fillEventDiv2(List<Block> blocks) {
    String uName = hhash.getRandomName(true);
    //Todo agregar el id a los bloques?
    for (int i = 0; i < blocks.length; i++) {
      Block block = blocks[i];

      DivElement selectedCol = _getHtmlBlockElement(block);
      DivElement event = selectedCol.querySelector(".event");
      event.classes.add(uName);
      event.style.position = "absolute";
      //  event.style.left = "${selectedCol.style.left}";
      //   event.style.top = "${int.parse(selectedCol.style.top.replaceAll("px", ""))}px";
      event.style.zIndex = "20";
      event.style.height = "${blockHeight - 5}px";
      event.style.marginTop = "2px";

      int paddingRight = 0;
      if (i == 0 && i == blocks.length - 1 && blocks.length == 1) {
        event.style.borderRadius = "2px";
        event.style.marginLeft = "2px";
        paddingRight = 5;
      } else {
        if (i == 0) {
          event.style.borderBottomLeftRadius = "2px";
          event.style.borderTopRightRadius = "2px";
          event.style.marginLeft = "2px";
          //  event.style.borderLeft = "1px solid #9FE1E7";
          paddingRight = 2;
        }

        if (i == blocks.length - 1) {
          event.style.borderBottomRightRadius = "2px";
          event.style.borderTopRightRadius = "2px";
          // event.style.borderRight = "1px solid #9FE1E7";

          paddingRight = 3;
        }
      }
/*      event.style.borderBottom = "1px solid #9FE1E7";
      event.style.borderTop = "1px solid #9FE1E7";*/

      event.style.width = "${(blockWidth - paddingRight)}px";

      // event.style.backgroundColor = "#C8F1F5";
      // event.style.backgroundColor = "#CECECE";
      //event.style.backgroundColor = "#F9F9F9";
      event.style.backgroundColor = "#f1f1f1";
      //event.style.backgroundColor = "#E6E6E6";
/*      event.style.backgroundImage =
      "repeating-linear-gradient(40deg,transparent,transparent 10px,#fff 10px,#fff 11px)";*/
    }
  }

  void setBlockBackgroundColor(DivElement block) {
    try {
      int col = int.parse(block.dataset["column"]);

      removeBlocksSelections();
      if (col <= colStart) {
        for (int i = colStart; i >= col; i--) {
          bodyDays[rowStart].blocks[i].styles["background-color"] = "#b3b3b3";
          colEnd = i;
        }
      } else {
        for (int i = colStart; i <= col; i++) {
          bodyDays[rowStart].blocks[i].styles["background-color"] = "#b3b3b3";
          colEnd = i;
        }
      }
    } catch (e) {
      //   print(e);
    }
  }

  void removeBlocksSelections() {
    bodyDays.forEach((day) {
      day.blocks.forEach((b) => b.styles.remove("background-color"));
    });
  }

  initOverlay() {
    schedulerOverlay.onClick.listen((event) {});
  }

  void onCancelDialog() {
    removeBlocksSelections();
    showBasicDialog = false;
  }

  void onAcceptDialog(ScheduleDetail schedulerDetail) {
    Block firstBlock = selectedBlocks.first;
    String asJson = dson.encode(schedulerDetail);
    ScheduleDetail cloned = dson.decode(asJson, new ScheduleDetail());
    cloned.startTime = parseDate(horarioList[selectedBlocks.first.column].dateTime);
    cloned.endTime =
        parseDate(horarioList[selectedBlocks.last.column].dateTime.add(new Duration(minutes: 15)));
    firstBlock.info = cloned;

    scheduleDetailList.add(cloned);
    fillFooter();
    new Timer(new Duration(milliseconds: 1), () {
      //fillEventDiv(firstBlock, selectedBlocks.length);
      removeBlocksSelections();
      fillEventDiv2(selectedBlocks);
      onEventPanelClick(_getHtmlBlockElement(selectedBlocks.first).querySelector(".event"));
      //  showBasicDialog = false;
    });
  }

  void fillFooter() {
    selectedBlocks2.forEach((b) {
      b.styles["background-color"] = "#DADFEA";
      b.styles["border-top"] = "1px solid white";
    });
    selectedBlocks2.last.styles["border-right"] = "1px solid white";
    // selectedBlocks2.first.styles["border-left"] = "1px solid white";
  }

  void showInputDialog() {
    showBasicDialog = true;
  }

  @ViewChild("schedulerScroll")
  ElementRef elementRef;
  DivElement footerScroll;

  bool isDragging = false;

  List<StreamSubscription> streamSubscription;

  void updateScheduler() {}

  DivElement _getHtmlBlockElement(Block block) {
    String id = "#block_${block.row}_${block.column}";
    return document.querySelector(id);
  }
}
