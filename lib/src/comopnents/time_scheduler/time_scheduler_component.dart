import 'dart:async';
import 'dart:html';
import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:intl/intl.dart';
import 'package:subject_scheduler/model/all_model.dart';
import 'package:dartson/dartson.dart';

@Component(
  selector: 'time-scheduler',
  styleUrls: const ['time_scheduler_component.css'],
  templateUrl: 'time_scheduler_component.html',
  directives: const [CORE_DIRECTIVES, materialDirectives],
  providers: const [materialProviders],
)
class TimeSchedulerComponent implements OnInit {
  Dartson dson = new Dartson.JSON();

  static const LEFT_BUTTON_CLICK = 0;

  static List<String> daysName = [
    "Lunes",
    "Martes",
    "Miercoles",
    "Jueves",
    "Viernes",
    "Sabado",
    "Domingo"
  ];

  ScheduleDetail schedulerInfo;
  bool showBasicDialog = false;
  List<Block> blocks = [];
  List<Day> days = [];
  List<Day> days2 = [];
  List<BlockTime> horarioList = [];

  DivElement schedulerOverlay;

  int blockWidth = 70;
  int blockHeight = 30;
  int firstColWidth = 1;
  int blocksCount = (16 * 4) + 1;
  int scrollLength;

  @ViewChild("calendarScheduler")
  ElementRef calendarScheduler;
  DivElement mainDiv;
  DivElement contentBody;
  DivElement contentHeader;
  DivElement bodyScroll;

  bool showOverlay = false;

  Map<String, String> popUpStyles;

  List<Block> selectedBlocks;
  List<Block> selectedBlocks2;

  @Input()
  ScheduleDetail schedulerDetail;

  @Input()
  List<ScheduleDetail> scheduleDetailList;

  StreamController _onRangeSelected = new StreamController.broadcast();

  @Output()
  Stream get rangeSelected => _onRangeSelected.stream;

  @override
  ngOnInit() {
    schedulerDetail = schedulerDetail != null ? schedulerDetail : new ScheduleDetail();
    scheduleDetailList = scheduleDetailList != null ? scheduleDetailList : [];
    //scheduler container
    mainDiv = calendarScheduler.nativeElement as DivElement;
    contentBody = mainDiv.querySelector(".content-body");
    bodyScroll = contentBody.querySelector(".body-scroll");
    popUpStyles = new Map();
    schedulerInfo =
        new ScheduleDetail(subject: "Ciencias", profesor: "Macos Enriquez", room: "443");

    //initOverlay();

    DateTime startDate = new DateTime(2017, 1, 1, 08, 00);
    for (int i = 0; i < blocksCount; i++) {
      Map<String, String> styles = new Map();

      styles["width"] = "${blockWidth}px";

      if (i == 0) {
        styles["width"] = "${firstColWidth}px";
        horarioList.add(
          new BlockTime(time: '', styles: styles),
        );
      } else {
        String startTime = parseDate(startDate);

        horarioList.add(
          new BlockTime(
              isAm: startTime.contains("AM"),
              time: startTime.replaceAll("AM", "").replaceAll("PM", "").replaceAll(":00", ""),
              dateTime: new DateTime.fromMillisecondsSinceEpoch(startDate.millisecondsSinceEpoch),
              isOclock: (i - 1) % 4 == 0,
              styles: styles),
        );
        startDate = startDate.add(new Duration(minutes: int.parse("15")));
      }
    }

    daysName.asMap().forEach((index, dayName) {
      Map<String, String> styles = new Map();
      styles["height"] = "${blockHeight}px";

      if (index == daysName.length - 1) {
        styles["height"] = "${blockHeight + 3}px";
        styles["border-bottom"] = "1px solid #DADFEA";
      }

      days.add(new Day(styles: styles, name: dayName, blocks: createBlocks(rowNumber: index)));
    });

    for (int i = 0; i < daysName.length; i++) {
      Map<String, String> styles = new Map();
      if (i == 0) {
        styles["max-width"] = "${firstColWidth}px";
      }
      days2.add(new Day(
          styles: new Map(), name: "", blocks: createBlocks(rowNumber: i, withStyle: false)));
    }

    initContentScroll();
    initDND();
  }

  List<Block> createBlocks({int rowNumber, withStyle = true}) {
    List<Block> blocks = [];
    int left = 0;
    for (int i = 0; i < blocksCount; i++) {
      int localBlockWidth = blockWidth;
      //first block 20px width
      if (i == 0) {
        localBlockWidth = firstColWidth;
      }

      Map<String, String> styles = new Map();

      if (withStyle) {
        styles["width"] = "${localBlockWidth}px";
        styles["height"] = "${blockHeight}px";
        styles["position"] = "absolute";
        styles["top"] = "${rowNumber * blockHeight}px";
        styles["left"] = "${left}px";

        String borderStyle = !(i % 4 == 0) ? "dashed" : "solid";
        styles["border-right"] = "1px ${borderStyle} #DADFEA";
      }

      blocks.add(new Block(styles: styles, row: rowNumber, column: i));

      left += localBlockWidth;
    }

    return blocks;
  }

  String parseDate(DateTime dt) {
    var formatter = new DateFormat.Hm();
    String formatted = formatter.format(dt);
    return formatted;
  }

  initContentScroll() {
    //scroll length in pixels
    scrollLength = (blocksCount * blockWidth) - blockWidth + firstColWidth;

    DivElement contentBody = mainDiv.querySelector(".content-body");

    if (contentBody == null) return;

    contentBody.style.height = "${(blockHeight + 6.5) * daysName.length}px";
    contentHeader = mainDiv.querySelector(".content-header");

    //set the final scroll lenght
    DivElement bodyScroll = contentBody.querySelector(".body-scroll");
    bodyScroll.style.width = "${scrollLength}px";

    DivElement headerScroll = contentHeader.querySelector(".header-scroll");
    headerScroll.style.width = "${scrollLength}px";
    contentBody.onScroll.listen((e) {
      contentHeader.scrollLeft = contentBody.scrollLeft;
    });

    contentBody.onMouseOver.listen((e) {
      if (isMouseDown) {
        print(e.page.x);
        if (e.page.x > contentBody.client.width) {
          contentBody.scrollLeft = contentBody.scrollLeft + 50;
        } else if (e.page.x < 400) {
          contentBody.scrollLeft = contentBody.scrollLeft - 50;
        }
      }
    });
  }

  bool isMouseDown = false;
  int rowStart = -1;
  int colStart = -1;
  int colEnd = -1;

  initDND() {
    if (contentBody == null) return;

    bodyScroll.onMouseDown.listen((e) {
      e.preventDefault();

      if (e.button == LEFT_BUTTON_CLICK &&
          e.target is DivElement &&
          (e.target as DivElement).classes.contains("subject-block")) {
        DivElement block = (e.target as DivElement);

        rowStart = int.parse(block.dataset["row"]);
        colStart = int.parse(block.dataset["column"]);

        setBlockBackgroundColor(block);
        isMouseDown = true;
      }
    });

    bodyScroll.onMouseUp.listen((e) {
      if (isMouseDown) {
        isMouseDown = false;

        int colStartAux = -1;
        if (colEnd < colStart) {
          colStartAux = colStart;
          colStart = colEnd;
          colEnd = colStartAux;
        }
        selectedBlocks = days[rowStart].blocks.getRange(colStart, colEnd + 1).toList();
        selectedBlocks2 = days2[rowStart].blocks.getRange(colStart, colEnd + 1).toList();

        print('rowStart $rowStart - '
            'colStart $colStart - '
            'colEnd $colEnd - '
            'selected cols ${selectedBlocks}');

        // showInputDialog();

        int firstCol = selectedBlocks.first.column;
        int lastCol = selectedBlocks.last.column;
        String dayName = days[selectedBlocks.first.row].name;
        print(schedulerDetail);
        schedulerDetail.startTime = parseDate(horarioList[firstCol].dateTime);
        schedulerDetail.endTime =
            parseDate(horarioList[lastCol].dateTime.add(new Duration(minutes: 15)));
        schedulerDetail.dayName = dayName;

        _onRangeSelected.add(schedulerDetail);
      }
    });

    bodyScroll.onMouseOver.listen((e) {
      if (isMouseDown) {
        DivElement block = (e.target as DivElement);
        setBlockBackgroundColor(block);
      }
    });
  }

  void fillEventDiv(Block startBlock, int blocksLength) {
    String id = "#block_${startBlock.row}_${startBlock.column}";
    DivElement selectedCol = mainDiv.querySelector(id);
    DivElement event = selectedCol.querySelector(".event");
    event.style.position = "absolute";
    event.style.borderRadius = "2px";
    //  event.style.left = "${selectedCol.style.left}";
    //   event.style.top = "${int.parse(selectedCol.style.top.replaceAll("px", ""))}px";
    event.style.zIndex = "20";
    event.style.height = "${blockHeight - 5}px";
    event.style.marginTop = "2px";
    event.style.width = "${(blockWidth * blocksLength) - 5}px";
    event.style.marginLeft = "2px";
    event.style.backgroundColor = "#F9F9F9";
  }

  void fillEventDiv2(List<Block> blocks) {
    for (int i = 0; i < blocks.length; i++) {
      Block block = blocks[i];
      String id = "#block_${block.row}_${block.column}";
      DivElement selectedCol = mainDiv.querySelector(id);
      DivElement event = selectedCol.querySelector(".event");
      event.style.position = "absolute";
      //  event.style.left = "${selectedCol.style.left}";
      //   event.style.top = "${int.parse(selectedCol.style.top.replaceAll("px", ""))}px";
      event.style.zIndex = "20";
      event.style.height = "${blockHeight - 5}px";
      event.style.marginTop = "2px";

      int paddingRight = 0;
      if (i == 0 && i == blocks.length - 1 && blocks.length == 1) {
        event.style.borderRadius = "2px";
        event.style.marginLeft = "2px";
        paddingRight = 5;
      } else {
        if (i == 0) {
          event.style.borderBottomLeftRadius = "2px";
          event.style.borderTopRightRadius = "2px";
          event.style.marginLeft = "2px";
          paddingRight = 2;
        }

        if (i == blocks.length - 1) {
          event.style.borderBottomRightRadius = "2px";
          event.style.borderTopRightRadius = "2px";

          paddingRight = 5;
        }
      }

      event.style.width = "${(blockWidth - paddingRight)}px";
      event.style.backgroundColor = "#F9F9F9";
      //   event.style.backgroundColor = "#E6E6E6";
/*      event.style.backgroundImage =
          "repeating-linear-gradient(40deg,transparent,transparent 10px,#fff 10px,#fff 15px)";*/
    }
  }

  void setBlockBackgroundColor(DivElement block) {
    removeBlocksSelections();
    int col = int.parse(block.dataset["column"]);
    if (col <= colStart) {
      for (int i = colStart; i >= col; i--) {
        days[rowStart].blocks[i].styles["background-color"] = "#b3b3b3";
        colEnd = i;
      }
    } else {
      for (int i = colStart; i <= col; i++) {
        days[rowStart].blocks[i].styles["background-color"] = "#b3b3b3";
        colEnd = i;
      }
    }
  }

  void removeBlocksSelections() {
    days.forEach((day) {
      day.blocks.forEach((b) => b.styles.remove("background-color"));
    });
  }

  initOverlay() {
    schedulerOverlay.onClick.listen((event) {});
  }

  void onCancelDialog() {
    removeBlocksSelections();
    showBasicDialog = false;
  }

  void onAcceptDialog() {
    Block firstBlock = selectedBlocks.first;
    String asJson = dson.encode(schedulerDetail);
    ScheduleDetail cloned = dson.decode(asJson, new ScheduleDetail());
    cloned.startTime = parseDate(horarioList[selectedBlocks.first.column].dateTime);
    cloned.endTime =
        parseDate(horarioList[selectedBlocks.last.column].dateTime.add(new Duration(minutes: 15)));
    firstBlock.info = cloned;

    scheduleDetailList.add(cloned);

    selectedBlocks2.forEach((b) {
      b.styles["background-color"] = "#DADFEA";
      b.styles["border-top"] = "1px solid white";
    });
    selectedBlocks2.last.styles["border-right"] = "1px solid white";
    // selectedBlocks2.first.styles["border-left"] = "1px solid white";

    new Timer(new Duration(milliseconds: 1), () {
      fillEventDiv(firstBlock, selectedBlocks.length);
      //fillEventDiv2(selectedBlocks);
      removeBlocksSelections();

      //  showBasicDialog = false;
    });
  }

  void showInputDialog() {
    showBasicDialog = true;
  }

  @ViewChild("schedulerScroll")
  ElementRef elementRef;
  DivElement footerScroll;

  bool isDragging = false;

  List<StreamSubscription> streamSubscription;

  void commitChanges() {
    onAcceptDialog();
  }

  void updateScheduler() {}
}
