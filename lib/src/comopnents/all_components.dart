//imports
import 'package:subject_scheduler/src/comopnents/time_chooser/time_chooser_component.dart';
import 'package:subject_scheduler/src/comopnents/time_scheduler/time_scheduler_component.dart';

//exports
export 'package:subject_scheduler/src/comopnents/time_scheduler/time_scheduler_component.dart';
export 'package:subject_scheduler/src/comopnents/time_chooser/time_chooser_component.dart';
export 'package:subject_scheduler/model/block_time.dart';

const List<Type> ALL_COMPONENTS = const [TimeSchedulerComponent, TimeChooserComponent];
